var app = angular.module('main',[]); 

app.controller('mainCntr',function($scope){
  $scope.theme={name:'city'};
 $scope.themes = [
   {name:'abstract'},
{name:'animals'},
{name:'business'},
{name:'cats'},
{name:'city'},
{name:'food'},
{name:'nightlife'},
{name:'fashion'},
{name:'people'},
{name:'nature'},
{name:'sports'},
{name:'technics'}];
});

app.directive('card', function () {
    return {
        restrict: 'AC',
        link: function (scope, element, attrs) {
        scope.$watch(attrs.delay, function (value) {
          element.css('-webkit-animation-delay','-'+attrs.delay+'s'); 
              element.css('-moz-animation-delay','-'+attrs.delay+'s'); 
           });                      
        }
    }
});
